class ProjectsController < ApplicationController
 def index
		@projects = Project.all
	end
	def show
		if params[:slug]
			@project = Project.find_by slug: params[:slug]
		else
			@project = Project.find(params[:id])
		end
	end
	def new
		@project = Project.new
	end
	def create
		@project = Project.new(project_params)
		if @project.save 
			redirect_to(projects_path)
		else
			render 'new'
		end
	end
	def edit
		@project = Project.find(params[:id])
	end
	def update
		@project = Project.find(params[:id])
		if @project.update_attributes(project_params)
			redirect_to(@project)
		else
			render 'edit'
		end
	end
	def destroy
		@project = Project.find(params[:id])
		@project.destroy
		redirect_to(companies_path)
	end
	private
	def project_params
		params.require(:project).permit(:name,:company_id,:default_rate,:slug)
	end
end
